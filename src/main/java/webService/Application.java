package webService;

import spark.Request;
import spark.Response;

import java.util.Optional;

import static spark.Spark.get;
import static spark.Spark.port;

public class Application {

    public static void main(String[] args) {

        String port = System.getenv("PORT");
        port(Optional.ofNullable(port).map(Integer::valueOf).orElse(4567));
        get("/hello", (Request req, Response res) -> "{\n  \"message\": \"Hello World\"\n}");

    }
}
